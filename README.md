# INSTRUCTIONS #

### Overview

The main task is to create a web app (with API counterpart) for a log monitoring system, that would track the users� logging in and out behavior and patterns.

### Functionalities and Specifications ###

1. The user should be able to create an account, both on the web app and mobile (through an API), by providing a username and a password. Once an account is created, the user can also edit the rest of his profile: First Name, Last Name, Birthdate, Position, Management (flag to check if he is part of the management), and Photo (can only be edited via web app). Once set, he can also view his profile and the info he entered on it.
1. The user should be able to log in both the web app and mobile, through the username-password pairing. On mobile, token authentication should be implemented. Whenever a user logs in, the system should be able to save the date and time when it happened, as well as if the user logs in through the web app or through mobile.
1. The user should also be able to log out on both platforms. Similarly, the system should also log when this logout event happened and on which platform.
1. The user should be also be able to track his logging patterns per month, by displaying the following information, on a table:
    * Date and time of login
    * Time spent (logout time - login time)
1. The logs should be paginated by month. At the bottom of the table, the total time spent by the user on that month should also be shown. This page should be accessed by clicking on the month�s name on the next feature.
1. The user also has the option to display a table of all the months and their corresponding total time spent. Again, if the month is clicked, it should display the logging pattern, as explained on the previous feature
1. Viewing of logging patterns must only be limited to logged-in users.
1. The web app should also have admin pages using Django admin. The admin must be able to view the following pages
    * A page where we can check all users and their corresponding total time spent
    * A list of all log-ins, which can be filtered by user and by month.
1. All calculations should be placed on utils.py. No computations should be found on the models, views, endpoints, serializers, etc. All utils.py must have 100% test coverage
1. The following URLpatterns should be followed:
    * {domain}/ - main homepage, must display link to displaying logging patterns and logout, if not logged in, must redirect to login page
    * {domain}/login - login page
    * {domain}/logout - logout page
    * {domain}/logs - list of months
    * {domain}/logs/{year}_{month} - logging patterns per month
    * {domain}/profile - profile page
    * {domain}/profile/edit - edit profile
    * {domain}/api - all api endpoints. Must follow the previous URLPatterns

###### Good Luck! ######